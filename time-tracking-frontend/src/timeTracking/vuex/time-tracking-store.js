import TIME_TRACKING_ACTIONS from "./time-tracking-actions";

const TIME_TRACKING_STORE = {
  state: () => ({
    trackingStepLastTime: {}
  }),

  getters: {
    trackingStepLastTime: state => state.trackingStepLastTime
  },

  actions: {
    saveTrackingStep({commit}, trackingStep) {
      commit(TIME_TRACKING_ACTIONS.saveTrackingStep, trackingStep)
    },

    removeTrackingStep({commit}) {
      commit(TIME_TRACKING_ACTIONS.removeTrackingStep)
    }
  },

  mutations: {
    saveTrackingStep(state, trackingStep) {
      state.trackingStepLastTime = trackingStep
    },

    removeTrackingStep(state) {
      state.trackingStepLastTime = {}
    }
  }
}

export default TIME_TRACKING_STORE
