const TIME_TRACKING_ACTIONS = {
  saveTrackingStep: 'saveTrackingStep',
  removeTrackingStep: 'removeTrackingStep'
}

export default TIME_TRACKING_ACTIONS
