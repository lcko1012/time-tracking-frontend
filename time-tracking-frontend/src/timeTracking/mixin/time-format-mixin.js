export default {
  padLeadingZero(time) {
    if(time < 10){
      return "0" + time;
    }
    else return time
  },

  getTimeInHour(time) {
    time = parseInt(time)
    let hour = parseInt(time / 3600)
    return this.padLeadingZero(hour)
  },

  getTimeInMinute(time) {
    time = parseInt(time)
    let hour = parseInt(this.getTimeInHour(time))
    let minute = parseInt((time - hour * 3600) / 60)
    return this.padLeadingZero(minute)
  },

  getTimeInSecond(time) {
    time = parseInt(time)
    let hour = parseInt(this.getTimeInHour(time))
    let minute = parseInt(this.getTimeInMinute(time))
    let second = parseInt(time - hour * 3600 - minute * 60)
    return this.padLeadingZero(second)
  },
}
