const BASE_URL = "http://localhost:8080"

export default {
  GET_STEP_LIST(id) {
    return `${BASE_URL}/step/by-user-story/${id}`
  },

  UPDATE_TIME_OF_STEP(id) {
    return `${BASE_URL}/step/${id}`
  },

  GET_USER_STORY_INFORMATION(id) {
    return `${BASE_URL}/user-story/${id}`
  },

  UPDATE_USER_STORY_TOTAL_TIME(id) {
    return `${BASE_URL}/user-story/${id}`
  }
}
