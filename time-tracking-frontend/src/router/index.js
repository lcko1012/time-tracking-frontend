import Vue from 'vue'
import Router from 'vue-router'
import Home from "../view/Home";
import TimeTracking from "../timeTracking/components/TimeTracking";
import Step from "../step/components/Step";
import Sprint from '../sprint/components/Sprint';
import UserStory from "../userStory/components/UserStory";
import {NotAuthenticated, Authenticated} from "../authentication/config/authentication-config";

Vue.use(Router)

const routes = [
  {
    path: '/',
    name: "home",
    component: Home,
    beforeEnter: NotAuthenticated
  },
  {
    path: '/sprint',
    name: "sprint",
    component: Sprint,
    beforeEnter: Authenticated
  },
  {
    path: '/userstory',
    name: "userstory",
    component: UserStory,
    props: true,
    beforeEnter: Authenticated
  },
  {
    path: '/step',
    name: "step",
    component: Step,
    beforeEnter: Authenticated
  },
  {
    path: '/timetracking/:id',
    name: "timetracking",
    props: true,
    component: TimeTracking,
    beforeEnter: Authenticated
  },
]

export default new Router({
  routes,
  mode: 'history'
})

