const BASE_URL = "http://localhost:8080/sprint"

export default {
  GET_ALL_SPRINTS: `${BASE_URL}`,
  CREATE_SPRINT: `${BASE_URL}`,
  DELETE_SPRINT(id) {
    return `${BASE_URL}/${id}`
  },
  UPDATE_SPRINT(id){
    return `${BASE_URL}/${id}`
  }
}
