export default {
  data() {
    return {
      requiredRule: v => !!v || 'is required',
      minimumChar: v => (v && v.length >= 8) || 'Minimum 8 character',
      integer: v => (Number(v) == v) || 'Must be a number!'
    }
  }
}
