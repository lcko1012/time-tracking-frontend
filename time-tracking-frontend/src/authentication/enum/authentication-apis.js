const BASE_URL = "http://localhost:8080/user"

export default {
  LOGIN: `${BASE_URL}/login`,
  REGISTER: `${BASE_URL}/register`,
}
