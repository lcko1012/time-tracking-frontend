import {store} from "../../store/store";

export const Authenticated = (to, from, next) => {
  if(store.getters.isLogged) {
    next()
    return
  }
  next("/")
}

export const NotAuthenticated = (to, from, next) => {
    if(!store.getters.isLogged) {
      next()
      return
    }
    next("/sprint")
}


