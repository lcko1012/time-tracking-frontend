const AUTHENTICATION_GETTERS = {
  userInfo: 'userInfo',
  isLogged: 'isLogged'
}

export default AUTHENTICATION_GETTERS
