import AUTHENTICATION_ACTIONS from "./authentication-actions";

const AUTHENTICATION_STORE =  {
  state: () => ({
    user: [],
    isLogged: false,
  }),

  actions: {
    login({commit}, user){
      commit(AUTHENTICATION_ACTIONS.login, user)
    }
  },

  mutations: {
    login(state, user) {
      state.isLogged = true
      state.user = user
    }
  },

  getters: {
    userInfo: state => state.user,
    isLogged: state => state.isLogged
  },
}

export default AUTHENTICATION_STORE
