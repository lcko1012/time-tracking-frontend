const BASE_URL = "http://localhost:8080/master-step"

export default  {
  GET_MASTER_STEP_LIST: `${BASE_URL}`,

  CREATE_MASTER_STEP: `${BASE_URL}`,

  UPDATE_MASTER_STEP(id){
    return `${BASE_URL}/${id}`
  },

  DElETE_MASTER_STEP(id) {
    return `${BASE_URL}/${id}`
  }
}
