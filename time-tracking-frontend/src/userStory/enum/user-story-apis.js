const BASE_URL = "http://localhost:8080/user-story"

export default {
  CREATE_USER_STORY: `${BASE_URL}`,

  GET_USER_STORY_BY_SPRINT_ID(id) {
    return `${BASE_URL}/list/${id}`
  },

  UPDATE_USER_STORY(id) {
    return `${BASE_URL}/${id}`;
  },

  DELETE_USER_STORY(id) {
    return `${BASE_URL}/delete/${id}`;
  },
}
