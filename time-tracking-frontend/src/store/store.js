import Vue from "vue";
import Vuex from "vuex";
import AUTHENTICATION_STORE from '../authentication/vuex/authentication-store'
import TIME_TRACKING_STORE from "../timeTracking/vuex/time-tracking-store";

Vue.use(Vuex);

export const store = new Vuex.Store({
  modules: {
    AUTHENTICATION_STORE,
    TIME_TRACKING_STORE
  }
})
